$(function () {
    var container = $(".animate-text")
    // Shuffle the container with custom text
    function shuffleText() {
        container.shuffleLetters({
            "text": "WEB-CAG"
        });
    }

    setInterval(shuffleText, 25000)

    shuffleText();
});
