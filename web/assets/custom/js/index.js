$("#sidebar-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $("#navbar").toggleClass("navbar-with-sidebar");
});

function showError(msg) {
    BootstrapDialog.alert({
        title: 'ERROR',
        message: msg,
        type: BootstrapDialog.TYPE_DANGER,
        buttonLabel: 'Ok'
    });
}

function showSuccess(msg) {
    BootstrapDialog.alert({
        title: 'SUCCESS',
        message: msg,
        type: BootstrapDialog.TYPE_SUCCESS,
        buttonLabel: 'Ok'
    });
}

function confirmDelete(id, func) {
    BootstrapDialog.show({
        message: "Are you sure you want to delete item with ID " + id + "?",
        title: "WARNING",
        type: BootstrapDialog.TYPE_WARNING,
        buttons: [{
            label: 'Yes',
            cssClass: 'btn-danger',
            action: function() {
                func();
            }
        }, {
            label: 'No',
            cssClass: 'btn-success',
            action: function(e) {
                e.close();
            }
        }]
    });
}

$(function () {
    function fillHeight() {
        if ($(window).width() > 750) {
            $('.fill-height').removeClass('hidden');
            $('.fill-width').removeAttr('style');
            $('.fill-height').css({
                top: 0,
                height: $(window).height()
            });
        } else {
            $('.fill-height').addClass('hidden');
            $('.fill-width').css({
                width: '100%'
            });
        }
    }

    $(window).resize(function () {
        fillHeight();
    });

    fillHeight();
});