if (typeof itemCount === 'undefined') {
    var itemCount = 0;
}

$(document).ready(function () {
    $('#btn-add-collection-item').click(function (e) {
        e.preventDefault();

        var collectionList = $('#collection-list');

        var newWidget = collectionList.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, itemCount);
        itemCount++;

        var newWidgetLi = $('<li class="collection-item"></li>').html(newWidget);

        var newWidgetDiv = $('<div class="col-sm-11 no-padding"></div>').html(newWidgetLi);

        var newItem = newWidgetDiv.add('<div class="col-sm-1">' +
            '<button type="button" class="btn btn-danger" onclick="$(this).parent().parent().remove();">Delete</button></div>');

        var div = $('<div></div>').html(newItem);

        div.appendTo(collectionList);
    });
});