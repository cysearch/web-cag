DROP TRIGGER IF EXISTS unique_teacher;
DROP TRIGGER IF EXISTS unique_student;
DROP TRIGGER IF EXISTS unique_admin;

DELIMITER //

CREATE TRIGGER unique_teacher BEFORE INSERT ON teacher
FOR EACH ROW BEGIN
  DECLARE c INT;
  SELECT COUNT(*) INTO c FROM (SELECT username FROM student UNION SELECT username FROM administrator UNION SELECT username FROM teacher) AS temp WHERE username=NEW.username;
  IF (c > 0) THEN
    SET NEW.username = NULL;
  END IF;
END//

CREATE TRIGGER unique_student BEFORE INSERT ON student
FOR EACH ROW BEGIN
  DECLARE c INT;
  SELECT COUNT(*) INTO c FROM (SELECT username FROM student UNION SELECT username FROM administrator UNION SELECT username FROM teacher) AS temp WHERE username=NEW.username;
  IF (c > 0) THEN
    SET NEW.username = NULL;
  END IF;
END//

CREATE TRIGGER unique_admin BEFORE INSERT ON administrator
FOR EACH ROW BEGIN
  DECLARE c INT;
  SELECT COUNT(*) INTO c FROM (SELECT username FROM student UNION SELECT username FROM administrator UNION SELECT username FROM teacher) AS temp WHERE username=NEW.username;
  IF (c > 0) THEN
    SET NEW.username = NULL;
  END IF;
END//


DELIMITER ;
