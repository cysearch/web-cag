php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:generate:entities AppBundle/Model/Institute
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load