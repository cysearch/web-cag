<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\AdministratorType;
use AdminBundle\Form\CourseSectionType;
use AdminBundle\Form\CourseType;
use AdminBundle\Form\SectionType;
use AdminBundle\Form\StudentType;
use AdminBundle\Form\TeacherType;
use AdminBundle\Form\UpdateCourseType;
use AdminBundle\Form\UpdateSectionType;
use AdminBundle\Form\UpdateStudentType;
use AdminBundle\Form\UpdateTeacherType;
use AppBundle\Model\Institute\Administrator;
use AppBundle\Model\Institute\Course;
use AppBundle\Model\Institute\Section;
use AppBundle\Model\Institute\Student;
use AppBundle\Model\Institute\Teacher;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin.dashboard")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    // /------------------------------------------COURSE----------------------------------------------------------------

    /**
     * @Route("/admin/course/create", name="admin.createCourse")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createCoursesAction(Request $request)
    {
        $course = new Course();
        // change object attributes
        $course->setCourseId('');
        $course->setTitle('');
        $course->setSemester(1);

        $form = $this->createForm(CourseType::class, $course);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($course);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Course created successfully.');
                return $this->redirectToRoute('admin.viewCourses');
            } catch (DBALException $e) {
                $form->get('courseId')->addError(new FormError('Course id should be unique.'));
            }
        }
        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/courses", name="admin.viewCourses")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewCoursesAction(Request $request)
    {
        $courses = $this->getDoctrine()
            ->getRepository('AppBundle:Course')
            ->findAll();

        return $this->render("@Admin/Courses/courses.html.twig", ['courses' => $courses]);
    }

    /**
     * @Route("/admin/course/delete/{id}", name="admin.deleteCourses")
     * @param Request $request
     * @param string $id
     * @return RedirectResponse
     */
    public function deleteCoursesAction(Request $request, string $id)
    {
        $courseIds = explode(",", $id);
        $success = false;
        $failure = false;

        foreach ($courseIds as $courseId) {
            $repo = $this->getDoctrine()
                ->getRepository('AppBundle:Course');
            $course = $repo->find($courseId);
            if (!$course) {
                throw $this->createNotFoundException('No course found for id ' . $id);
            }
            $em = $this->getDoctrine()->getManager();
            try {
                $em->remove($course);
                $em->flush();
                $success[] = $courseId;
            } catch (PDOException $ex) {
                $failure[] = $courseId;
            } catch (DBALException $ex) {
                $failure[] = $courseId;
            } catch (\Exception $ex) {
                $failure[] = $courseId;
            }
        }

        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Course with id ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting course with id ' . join(',', $failure) . '. Dependencies exist.');
        }

        return new RedirectResponse($this->generateUrl('admin.viewCourses'));
    }

    /**
     * @Route("/admin/course/update/{id}", name="admin.updateCourse")
     * @param Request $request
     * @param string $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateCoursesAction(Request $request, string $id)
    {
        $course = $this->getDoctrine()
            ->getRepository('AppBundle:Course')
            ->find($id);

        if (!$course) {
            throw $this->createNotFoundException('No course found for id ' . $id);
        }

        $form = $this->createForm(UpdateCourseType::class, $course);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($course);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Course updated successfully.');
            } catch (DBALException $e) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Failed updating course.');
            }
            return $this->redirectToRoute('admin.viewCourses');
        }
        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    // /-----------------------------------------SECTION----------------------------------------------------------------

    /**
     * @Route("/admin/section/create", name="admin.createSection")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createSectionAction(Request $request)
    {
        $section = new Section();

        $form = $this->createForm(SectionType::class, $section);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($section);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Section created successfully.');
            } catch (DBALException $e) {
                $form->addError(new FormError('The section should be unique.'));
            }
            return $this->redirectToRoute('admin.viewSections', ['courseId' => $section->getCourse()->getCourseId()]);
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/section/create/{courseId}", name="admin.createCourseSection")
     * @param Request $request
     * @param string $courseId
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createCourseSectionAction(Request $request, string $courseId)
    {
        $course = $this->getDoctrine()
            ->getRepository('AppBundle:Course')
            ->find($courseId);

        if (!$course) {
            throw $this->createNotFoundException('No course found for id ' . $courseId);
        }

        $section = new Section();

        $section->setCourse($course);

        $form = $this->createForm(CourseSectionType::class, $section);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($section);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Section created successfully.');
                return $this->redirectToRoute('admin.viewSections', ['courseId' => $section->getCourse()->getCourseId()]);
            } catch (DBALException $e) {
                $form->addError(new FormError('The section should be unique.'));
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/section/update/{id}", name="admin.updateSection")
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateSectionAction(Request $request, int $id)
    {
        $section = $this->getDoctrine()
            ->getRepository('AppBundle:Section')
            ->find($id);
        if (!$section) {
            throw $this->createNotFoundException('Section not found for id ' . $id);
        }

        $form = $this->createForm(UpdateSectionType::class, $section);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($section);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Section updated successfully.');
                return $this->redirectToRoute('admin.viewSections', ['courseId' => $section->getCourse()->getCourseId()]);
            } catch (DBALException $e) {
                $form->addError(new FormError('The section should be unique.'));
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/admin/course/{courseId}/sections/", name="admin.viewSections")
     * @param string $courseId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewSectionsAction(string $courseId)
    {
        $course = $this->getDoctrine()
            ->getRepository('AppBundle:Course')
            ->find($courseId);

        $sections = $this->getDoctrine()
            ->getRepository('AppBundle:Section')
            ->findBy(['course' => $course]);

        return $this->render("@Admin/Sections/sections.html.twig", ['sections' => $sections, 'course' => $course]);
    }

    /**
     * @Route("/admin/sections/", name="admin.viewAllSections")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAllSectionsAction()
    {
        $sections = $this->getDoctrine()
            ->getRepository('AppBundle:Section')
            ->findAll();
        return $this->render("@Admin/Sections/allsections.html.twig", ['sections' => $sections]);
    }


    /**
     * @Route("/admin/section/delete/{id}", name="admin.deleteSection")
     * @param Request $request
     * @param string $id
     * @return RedirectResponse
     */
    public function deleteSectionAction(Request $request, string $id)
    {
        $courseId = $this->deleteSections($request, $id);
        return $this->redirectToRoute('admin.viewSections', ['courseId' => $courseId]);
    }

    /**
     * @Route("/admin/section/delete_back_all/{id}", name="admin.deleteSectionReturnToAllSections")
     * @param Request $request
     * @param string $id
     * @return RedirectResponse
     */
    public function deleteSectionReturnToAllSections(Request $request, string $id)
    {
        $this->deleteSections($request, $id);
        return $this->redirectToRoute('admin.viewAllSections');
    }

    /**
     * @param Request $request
     * @param string $id
     * @return string CourseId
     */
    public function deleteSections(Request $request, string $id)
    {
        $sectionIds = explode(',', $id);
        $success = false;
        $failure = false;
        foreach ($sectionIds as $sectionId) {
            $repo = $this->getDoctrine()
                ->getRepository('AppBundle:Section');
            $section = $repo->find($sectionId);
            if (!$section) {
                throw $this->createNotFoundException('No section found for id ' . $id);
            }
            $courseId = $section->getCourse()->getCourseId();
            $em = $this->getDoctrine()->getManager();
            try {
                $em->remove($section);
                $em->flush();
                $success[] = $sectionId;
            } catch (DBALException $ex) {
                $failure[] = $sectionId;

            } catch (\Exception $ex) {
                $failure[] = $sectionId;
            }
        }

        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Section with ids ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting sections with id ' . join(',', $failure) . '. Dependencies exist.');
        }
        return $courseId;
    }

    // /-----------------------------------------TEACHER----------------------------------------------------------------

    /**
     * @Route("/admin/teacher/create", name="admin.createTeacher")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createTeacherAction(Request $request)
    {
        $teacher = new Teacher();

        $form = $this->createForm(TeacherType::class, $teacher);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $teacher->encodePassword($this->container);
                $em = $this->getDoctrine()->getManager();
                $em->persist($teacher);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Created the teacher successfully.');
                return $this->redirectToRoute('admin.viewTeachers');
            } catch (DBALException $ex) {
                $form->get('username')->addError(new FormError('Username should be unique.'));
                $form->get('email')->addError(new FormError('E-mail should be unique.'));
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/teachers", name="admin.viewTeachers")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewTeachersAction()
    {
        $teachers = $this->getDoctrine()
            ->getRepository('AppBundle:Teacher')
            ->findAll();

        return $this->render("@Admin/Teacher/teachers.html.twig", ['teachers' => $teachers]);
    }

    /**
     * @Route("/admin/teacher/update/{id}", name="admin.updateTeachers")
     * @param Request $request
     * @param string $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateTeachersAction(Request $request, string $id)
    {
        $teacher = $this->getDoctrine()
            ->getRepository('AppBundle:Teacher')
            ->find($id);

        $form = $this->createForm(UpdateTeacherType::class, $teacher);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($teacher);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated the teacher successfully.');
                return $this->redirectToRoute('admin.viewTeachers');
            } catch (DBALException $ex) {
                $form->get('username')->addError(new FormError('Username should be unique.'));
                $form->get('email')->addError(new FormError('E-mail should be unique.'));
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/teacher/delete/{username}", name="admin.deleteTeachers")
     * @param Request $request
     * @param string $username
     * @return RedirectResponse
     */
    public function deleteTeachersAction(Request $request, string $username)
    {
        $usernames = explode(',', $username);
        $success = false;
        $failure = false;
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Teacher');
        foreach ($usernames as $user) {
            $teacher = $repo->find($user);

            if (!$teacher) {
                throw $this->createNotFoundException(
                    'No teacher found for username ' . $username
                );
            }

            $em = $this->getDoctrine()->getManager();
            try {
                $em->remove($teacher);
                $em->flush();
                $success[] = $user;
            } catch (DBALException $ex) {
                $failure[] = $user;
            } catch (\Exception $ex) {
                $failure[] = $user;
            }
        }
        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Teacher with usernames ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting teacher with username ' . join(',', $failure) . '. Dependencies exist.');
        }
        return new RedirectResponse($this->generateUrl('admin.viewTeachers'));
    }


    // /-----------------------------------------STUDENT----------------------------------------------------------------
    /**
     * @Route("/admin/student/create", name="admin.createStudent")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createStudentAction(Request $request)
    {
        $student = new Student();

        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $student->encodePassword($this->container);
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($student);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Created the student successfully.');
                return $this->redirectToRoute('admin.viewStudents');
            } catch (DBALException $ex) {
                $form->get('username')->addError(new FormError('Username should be unique.'));
                $form->get('email')->addError(new FormError('E-mail should be unique.'));
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/students/{batch}", name="admin.viewStudentsByBatch")
     * @param string $batch
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewStudentsByBatchAction(string $batch)
    {
        $students = $this->getDoctrine()
            ->getRepository('AppBundle:Student')
            ->findBy(['batch' => $batch]);

        return $this->render("@Admin/Student/students.html.twig", ['students' => $students]);
    }

    /**
     * @Route("/admin/students", name="admin.viewStudents")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewStudentsAction()
    {
        $students = $this->getDoctrine()
            ->getRepository('AppBundle:Student')
            ->findAll();

        return $this->render("@Admin/Student/students.html.twig", ['students' => $students]);
    }

    /**
     * @Route("/admin/student/update/{username}", name="admin.updateStudents")
     * @param Request $request
     * @param string $username
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateStudentAction(Request $request, string $username)
    {
        $student = $this->getDoctrine()
            ->getRepository('AppBundle:Student')
            ->find($username);

        $form = $this->createForm(UpdateStudentType::class, $student);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($student);
                $em->flush();

                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated the student successfully.');
                return $this->redirectToRoute('admin.viewStudents');
            } catch (DBALException $ex) {
                $form->get('sections')->addError(new FormError('Each section should be unique.'));
                $form->get('username')->addError(new FormError('Username should be unique.'));
                $form->get('email')->addError(new FormError('E-mail should be unique.'));
            }
        }

        return $this->render('@Admin/Student/studentForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/student/delete/{username}", name="admin.deleteStudent")
     * @param Request $request
     * @param string $username
     * @return RedirectResponse
     */
    public function deleteStudentAction(Request $request, string $username)
    {
        $usernames = explode(',', $username);
        $success = false;
        $failure = false;
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Student');
        foreach ($usernames as $user) {
            $student = $repo->find($user);
            if (!$student) {
                throw $this->createNotFoundException('No student found for username ' . $user);
            }
            $em = $this->getDoctrine()->getManager();
            try {
                $em->remove($student);
                $em->flush();
                $success[] = $user;
            } catch (DBALException $ex) {
                $failure[] = $user;
            } catch (\Exception $ex) {
                $failure[] = $user;
            }
        }
        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Student with usernames ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting student with username ' . join(',', $failure) . '. Dependencies exist.');
        }
        return new RedirectResponse($this->generateUrl('admin.viewStudents'));
    }

    // /---------------------------------------ENROLMENTS---------------------------------------------------------------

    // Managed with update user as Collection Type //

    // /------------------------------------------PROFILE---------------------------------------------------------------

    /**
     * @Route("/admin/profile/update", name="admin.updateProfile")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateProfileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Administrator $admin */
        $admin = $this->get('security.token_storage')->getToken()->getUser();

        $entity = clone($admin);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $form = $this->createForm(AdministratorType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $temp = clone($admin);

            $admin->setUsername($entity->getUsername());
            $admin->setEmail($entity->getEmail());
            $admin->setLastName($entity->getLastName());
            $admin->setFirstName($entity->getFirstName());
            $admin->setPassword($entity->getPassword());

            // Encode password
            $admin->encodePassword($this->container);

            try {
                $em->persist($admin);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated successfully.');
                return $this->redirectToRoute('admin.dashboard');
            } catch (DBALException $ex) {
                $form->get('username')->addError(new FormError('There is a person with the same username.'));
                $form->get('email')->addError(new FormError('There is a person with the same e-mail address.'));

                $admin->setUsername($temp->getUsername());
                $admin->setEmail($temp->getEmail());
                $admin->setLastName($temp->getLastName());
                $admin->setFirstName($temp->getFirstName());
                $admin->setPassword($temp->getPassword());
            }
        }

        return $this->render('@Admin/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}