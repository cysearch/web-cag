<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/1/2016
 * Time: 9:10 AM
 */

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('courseId', TextType::class)
            ->add('title', TextType::class)
            ->add('semester', IntegerType::class)
            ->add('submit', SubmitType::class, array('label' => 'Create'))
        ;
    }
}