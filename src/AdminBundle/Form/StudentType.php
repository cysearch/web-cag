<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/1/2016
 * Time: 9:10 AM
 */

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class StudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'User name'))
            ->add('firstName', TextType::class, array('label' => 'First name'))
            ->add('lastName', TextType::class, array('label' => 'Last name'))
            ->add('email', EmailType::class, array('label' => 'E-Mail'))
            ->add('batch', TextType::class, array('label' => 'Batch'))
            ->add('password', PasswordType::class, array('label' => 'Password'))
            ->add('isActive', CheckboxType::class, array('label' => 'Active',
                'required' => false,))
            ->add('submit', SubmitType::class, array('label' => 'Create'));
    }
}