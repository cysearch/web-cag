<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/1/2016
 * Time: 9:10 AM
 */

namespace AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CourseSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year')
            ->add('batch')
            ->add('teacher', EntityType::class, array(
                'class' => 'AppBundle:Teacher',
                'choice_label' => 'username',
            ))
            ->add('submit', SubmitType::class, array('label' => 'Create'))
        ;
    }
}