<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/1/2016
 * Time: 9:10 AM
 */

namespace AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UpdateStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'User name', 'disabled' => true))
            ->add('firstName', TextType::class, array('label' => 'First name'))
            ->add('lastName', TextType::class, array('label' => 'Last name'))
            ->add('email', EmailType::class, array('label' => 'E-Mail'))
            ->add('batch', TextType::class, array('label' => 'Batch'))
            ->add('sections', CollectionType::class, array(
                'entry_type' => EntityType::class,
                'entry_options'=>['class' => 'AppBundle:Section',
                'choice_label' => 'to string'],
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,))
            ->add('isActive', CheckboxType::class, array('label' => 'Active', 'required' => false,))
            ->add('update', SubmitType::class, array('label' => 'Update'));
    }
}