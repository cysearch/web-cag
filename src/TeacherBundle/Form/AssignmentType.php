<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/2/2016
 * Time: 4:40 PM
 */

namespace TeacherBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;

class AssignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextType::class)
            ->add('questions', CollectionType::class, array(
                'entry_type' => EntityType::class,
                'entry_options' => [
                    'class' => 'AppBundle:Question',
                    'choice_label' => 'toString',
                    'query_builder' => function (EntityRepository $er) use($options)  {
                        $r = $options['data'];
                        return $er->createQueryBuilder('q')
                            ->where('q.course = ?1')
                            ->setParameter(1, $r->getSection()->getCourse());
                    }],
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,))
            ->add('attemptsAllowed', IntegerType::class)
            ->add('enableDate', DateTimeType::class)
            ->add('disableDate', DateTimeType::class)
            ->add('totalMarksAllowed', IntegerType::class)
            ->add('submit', SubmitType::class, array('label' => 'Update'));
    }
}