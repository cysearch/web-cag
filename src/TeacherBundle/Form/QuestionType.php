<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 5/2/2016
 * Time: 4:40 PM
 */

namespace TeacherBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('course', EntityType::class, array(
                'class' => 'AppBundle:Course',
                'choice_label' => 'courseId',
                'disabled' => true
            ))
            ->add('description', TextareaType::class)
            ->add('testCases', TextareaType::class, array('required' => false))
            ->add('submit', SubmitType::class, array('label' => 'Update'));
    }
}