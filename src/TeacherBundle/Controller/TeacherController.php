<?php

namespace TeacherBundle\Controller;

use AppBundle\Model\Grader\PythonSourceCodeTester;
use AppBundle\Model\Institute\Assignment;
use AppBundle\Model\Institute\Attempt;
use AppBundle\Model\Institute\Course;
use AppBundle\Model\Institute\Question;
use AppBundle\Model\Institute\Section;
use AppBundle\Model\Institute\Student;
use AppBundle\Model\Institute\Teacher;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TeacherBundle\Form\AssignmentType;
use TeacherBundle\Form\QuestionType;
use TeacherBundle\Form\TeacherType;

class TeacherController extends Controller
{
    /**
     * @Route("/teacher", name="teacher.dashboard")
     */
    public function indexAction()
    {
        return $this->render('TeacherBundle:Default:index.html.twig');
    }

    // /-----------------------------------------SECTION----------------------------------------------------------------

    /**
     * @Route("/teacher/sections", name="teacher.viewSections")
     */
    public function viewSectionsAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('@Teacher/Sections/sections.html.twig', ['sections' => $user->getSections()]);
    }


    // /---------------------------------------ASSIGNMENT---------------------------------------------------------------

    /**
     * @Route("/teacher/section/{sectionId}/assignments", name="teacher.viewAssignments")
     * @param int $sectionId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function viewAssignmentsAction(int $sectionId)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();;
        /** @var Section $section */
        foreach ($user->getSections() as $section) {
            if ($section->getSectionId() == $sectionId) {
                /**@var array */
                $assignments = $section->getAssignments();
                return $this->render('@Teacher/Assignments/assignments.html.twig', ['assignments' => $assignments, 'sectionId' => $sectionId]);
            }
        }
        throw $this->createNotFoundException('No section found with sectionID ' . $sectionId . ' available for the user.');
    }

    /**
     * @Route("/teacher/assignment/{assignmentId}/update", name="teacher.updateAssignment")
     * @param Request $request
     * @param string $assignmentId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAssignmentsAction(Request $request, string $assignmentId)
    {
        // Get the user
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $username = $usr->getUsername();
        // Get the requested assignment from the database
        $repo = $this->getDoctrine()->getRepository('AppBundle:Assignment');
        $assignment = $repo->find($assignmentId);
        // Is there any assignment?
        if (!$assignment) {
            throw $this->createNotFoundException('No assignment found with AssignmentId ' . $assignmentId);
        }
        // Is the assignment owned bt the requesting party to update
        if ($assignment->getSection()->getTeacher()->getUsername() != $username) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(AssignmentType::class, $assignment);

        $form->handleRequest($request);

        // Store in the database if valid form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($assignment);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated the Assignment successfully.');
                return $this->redirectToRoute('teacher.viewAssignments', ['sectionId' => $assignment->getSection()->getSectionId()]);
            } catch (DBALException $ex) {
                $form->get('questions')->addError(new FormError('Each question should be unique.'));
            } catch (\Exception $ex) {
                $form->get('questions')->addError(new FormError('Each question should be unique.'));
            }
        }
        return $this->render('@Teacher/Assignments/updateAssignment.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/teacher/section/{sectionId}/assignment/create", name="teacher.createAssignment")
     * @param Request $request
     * @param int $sectionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAssignmentsAction(Request $request, int $sectionId)
    {
        // Get the user
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var Section $section */
        $section = null;
        foreach ($user->getSections() as $s) {
            if ($s->getSectionId() == $sectionId) {
                $section = $s;
            }
        }
        // Is the assignment owned bt the requesting party to update
        if (!$section) {
            throw $this->createNotFoundException("Section not found for the current user.");
        }

        $assignment = new Assignment();
        $assignment->setSection($section);
        $assignment->setDisableDate(new \DateTime('now'));
        $assignment->setEnableDate(new \DateTime('now'));

        $form = $this->createForm(AssignmentType::class, $assignment);

        $form->handleRequest($request);

        // Store in the database if valid form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($assignment);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Assignment created successfully.');
                return $this->redirectToRoute('teacher.viewAssignments', ['sectionId' => $assignment->getSection()->getSectionId()]);
            } catch (DBALException $ex) {
                $form->get('questions')->addError(new FormError('Each question should be unique.'));
            } catch (\Exception $ex) {
                $form->get('questions')->addError(new FormError('Each question should be unique.'));
            }
        }

        return $this->render('@Teacher/Assignments/updateAssignment.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/teacher/assignment/{assignmentIds}/delete", name="teacher.deleteAssignments")
     * @param Request $request
     * @param string $assignmentIds
     * @return RedirectResponse
     * @internal param string $questionIDs
     */
    public function deleteAssignmentsAction(Request $request, string $assignmentIds)
    {
        $assignments = explode(',', $assignmentIds);
        $success = false;
        $failure = false;
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Assignment');
        $assignment = false;
        foreach ($assignments as $temp) {
            $assignment = $repo->find($temp);
            if (!$assignment) {
                throw $this->createNotFoundException('No Assignment found for id ' . $temp);
            }

            $em = $this->getDoctrine()->getManager();

            // Get the user
            /** @var Teacher $teacher */
            $teacher = $this->get('security.token_storage')->getToken()->getUser();

            // Is the question owned by the requesting party to update
            $ownAssignment = $teacher->getSections()->contains($assignment->getSection());
            if (!$ownAssignment) throw new AccessDeniedException();
            try {
                $em->remove($assignment);
                $em->flush();
                $success[] = $temp;
            } catch (DBALException $ex) {
                $failure[] = $temp;
            } catch (\Exception $ex) {
                $failure[] = $temp;
            }
        }
        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Assignment with IDs ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting assignments with IDs ' . join(',', $failure) . '. Dependencies exist.');
        }
        if ($assignment) {
            return new RedirectResponse($this->generateUrl('teacher.viewAssignments', ['sectionId' => $assignment->getSection()->getSectionId()]));
        } else {
            return new RedirectResponse($this->generateUrl('teacher.viewSections'));
        }
    }


    /**
     * @Route("/teacher/assignment/{assignmentID}/updateAttempts", name="teacher.updateAttemptGrades")
     * @param Request $request
     * @param string $assignmentID
     * @return RedirectResponse
     * @internal param string $questionIDs
     */
    public function updateAttemptGradesAction(Request $request, string $assignmentID)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Assignment');
        $assignment = $repo->find($assignmentID);
        if (!$assignment) {
            throw $this->createNotFoundException('No Assignment found for id ' . $assignmentID);
        }
        // Get the user
        /** @var Teacher $teacher */
        $teacher = $this->get('security.token_storage')->getToken()->getUser();
        // Is the question owned by the requesting party to update
        $ownAssignment = $teacher->getSections()->contains($assignment->getSection());
        if (!$ownAssignment) throw new AccessDeniedException();
        try {
            $assignment->updateAttemptedGrades($this->getDoctrine());
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Attempts updated successfully.');
        } catch (DBALException $ex) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed updating attempts.');
        } catch (\Exception $ex) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed updating attempts.');
        }
        if ($assignment) {
            return new RedirectResponse($this->generateUrl('teacher.viewAssignments', ['sectionId' => $assignment->getSection()->getSectionId()]));
        } else {
            return new RedirectResponse($this->generateUrl('teacher.viewSections'));
        }
    }


    // /------------------------------------------COURSE----------------------------------------------------------------

    /**
     * @Route("/teacher/courses", name="teacher.viewCourses")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewCoursesAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var Section $section */
        $courses = [];
        foreach ($user->getSections() as $section) {
            if (!in_array($section->getCourse(), $courses)) {
                $courses[] = $section->getCourse();
            }
        }
        return $this->render('@Teacher/Courses/courses.html.twig',
            ['courses' => $courses]);
    }

    // /----------------------------------------QUESTIONS---------------------------------------------------------------

    /**
     * @Route("/teacher/course/{courseId}/question/create", name="teacher.createQuestion")
     * @param Request $request
     * @param string $courseId
     * @return null
     */
    public function createQuestionAction(Request $request, string $courseId)
    {
        // Get the user
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var Section $section */
        foreach ($user->getSections() as $section) {

            /** @var Course $course */
            $course = $section->getCourse();
            if ($course->getCourseId() == $courseId) {
                $question = new Question();
                $question->setCourse($course);
                $form = $this->createForm(QuestionType::class, $question);

                $form->handleRequest($request);

                // Store in the database if valid form is submitted
                if ($form->isSubmitted() && $form->isValid()) {
                    $sourceCodeTester = new PythonSourceCodeTester();
                    $testResults = $sourceCodeTester->checkSyntax($question->getTestCases());
                    if ($testResults['out'] != "" || $testResults['err'] != "") {
                        $splitErr = explode(',', $testResults['err'], 2);
                        if (sizeof($splitErr) == 2) {
                            $error = explode(' ', $splitErr[1], 4);
                            $error_line = $error[2] - 38;
                            $err_msg = $error[3];
                            $form->get('testCases')->addError(new FormError('Unable to compile the test code. Please check the tests again.' . " line no: " . $error_line . " " . $err_msg));
                        } else {
                            $form->get('testCases')->addError(new FormError('Unable to compile the test code. Please check the tests again.'));
                        }
                    } else {
                        try {
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($question);
                            $em->flush();
                            $request->getSession()
                                ->getFlashBag()
                                ->add('success', 'Created the question successfully.');
                            return $this->redirectToRoute('teacher.viewQuestions', ['courseId' => $courseId]);
                        } catch (DBALException $ex) {
                            $request->getSession()
                                ->getFlashBag()
                                ->add('error', 'Unable to create question. Please contact the administrator.');
                        } catch (\Exception $ex) {
                            $request->getSession()
                                ->getFlashBag()
                                ->add('error', 'Unable to create question. Please contact the administrator.');
                        }
                    }
                }

                return $this->render('@Teacher/Question/questionForm.html.twig', array(
                    'form' => $form->createView(),
                ));
            }
        }

        throw $this->createNotFoundException("The course does not exist.");
    }

    /**
     * @Route("/teacher/question/{questionID}/update", name="teacher.updateQuestion")
     * @param Request $request
     * @param string $questionID
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param string $assignmentId
     */
    public function updateQuestionAction(Request $request, string $questionID)
    {
        // Get the user
        /** @var Teacher $teacher */
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        // Get the requested assignment from the database
        $repo = $this->getDoctrine()->getRepository('AppBundle:Question');
        $question = $repo->find($questionID);

        // Is there any assignment?
        if (!$question) {
            throw $this->createNotFoundException('No question found with Question ID ' . $questionID);
        }

        // Is the question owned by the requesting party to update
        $ownQuestion = false;
        foreach ($teacher->getSections() as $section) {
            if ($question->getCourse() == $section->getCourse()) {
                $ownQuestion = true;
                break;
            }
        }
        if (!$ownQuestion) throw new AccessDeniedException();

        $form = $this->createForm(QuestionType::class, $question);

        $form->handleRequest($request);

        // Store in the database if valid form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            $sourceCodeTester = new PythonSourceCodeTester();
            $testResults = $sourceCodeTester->checkSyntax($question->getTestCases());
            if ($testResults['out'] != "" || $testResults['err'] != "") {
                $splitErr = explode(',', $testResults['err'], 2);
                if (sizeof($splitErr) == 2) {
                    $error = explode(' ', $splitErr[1], 4);
                    $error_line = $error[2] - 38;
                    $err_msg = $error[3];
                    $form->get('testCases')->addError(new FormError('Unable to compile the test code. Please check the tests again.' . " line no: " . $error_line . " " . $err_msg));
                } else {
                    $form->get('testCases')->addError(new FormError('Unable to compile the test code. Please check the tests again.'));
                }
            } else {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($question);
                    $em->flush();
                    $request->getSession()
                        ->getFlashBag()
                        ->add('success', 'Updated the question successfully.');

                    return $this->redirectToRoute('teacher.viewQuestions', ['courseId' => $question->getCourse()->getCourseId()]);
                } catch (DBALException $ex) {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Unable to update question. Please contact the administrator.');
                } catch (\Exception $ex) {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'Unable to update question. Please contact the administrator.');
                }
            }
        }

        return $this->render('@Teacher/Question/questionForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/teacher/course/{courseId}/questions", name="teacher.viewQuestions")
     *
     * @param string $courseId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewQuestionsAction(string $courseId)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /** @var Section $section */
        $questions = [];
        foreach ($user->getSections() as $section) {
            if ($section->getCourse()->getCourseId() == $courseId) {
                $questions = $section->getCourse()->getQuestions();
            }
        }

        return $this->render('@Teacher/Question/questions.html.twig',
            ['questions' => $questions, 'courseId' => $courseId]);
    }

    /**
     * @Route("/teacher/question/{questionIDs}/delete", name="teacher.deleteQuestion")
     * @param Request $request
     * @param string $questionIDs
     * @return RedirectResponse
     */
    public function deleteQuestionAction(Request $request, string $questionIDs)
    {
        $questions = explode(',', $questionIDs);
        $success = false;
        $failure = false;
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Question');
        $course = null;
        foreach ($questions as $temp) {
            $question = $repo->find($temp);
            if (!$question) {
                throw $this->createNotFoundException('No Question found for id ' . $temp);
            }

            $em = $this->getDoctrine()->getManager();

            // Get the user
            /** @var Teacher $teacher */
            $teacher = $this->get('security.token_storage')->getToken()->getUser();

            // Is the question owned by the requesting party to update
            $ownQuestion = false;
            foreach ($teacher->getSections() as $section) {
                $course = $question->getCourse();
                if ($course == $section->getCourse()) {
                    $ownQuestion = true;
                    break;
                }
            }
            if (!$ownQuestion) throw new AccessDeniedException();

            try {
                $em->remove($question);
                $em->flush();
                $success[] = $temp;
            } catch (DBALException $ex) {
                $failure[] = $temp;
            } catch (\Exception $ex) {
                $failure[] = $temp;
            }
        }
        if ($success) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Question with question IDs ' . join(',', $success) . ' deleted successfully.');
        }
        if ($failure) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Failed deleting questions with question IDs ' . join(',', $failure) . '. Dependencies exist.');
        }

        if ($course) {
            return new RedirectResponse($this->generateUrl('teacher.viewQuestions', ['courseId' => $course->getCourseId()]));
        } else {
            return new RedirectResponse($this->generateUrl('teacher.viewCourses'));
        }
    }

    // /------------------------------------------REPORTS---------------------------------------------------------------

    /**
     * @Route("/teacher/reports", name="teacher.viewReports")
     * @return Response
     * @internal param int $assignmentId
     */
    public function viewReportsAction()
    {
        /** @var Teacher $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /** @var array $sections */
        $sections = $user->getSections();
        $courses = array();
        /** @var Section $section */
        foreach ($sections as $section) {
            if ($section->getTeacher()->getUsername() == $user->getUsername()) {
                if (!in_array($section->getCourse(), $courses)) $courses[] = $section->getCourse();
            }
        }
        return $this->render(
            '@Teacher/Reports/report.html.twig',
            ['courses' => $courses]
        );
    }

    /**
     * @Route("/teacher/sections/{courseId}/get/", name="teacher.getSections")
     * @param string $courseId
     * @return JsonResponse
     */
    public function getSectionsAction(string $courseId)
    {
        /** @var Teacher $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Section');
        /** @var array $sections */
        $sections = $repo->findBy(['course' => $courseId]);
        $sectionIds = [];
        /** @var Section $section */
        foreach ($sections as $section) {
            if ($section->getTeacher()->getUsername() != $user->getUsername()) {
                throw new AccessDeniedException();
            }
            $sectionIds[$section->getSectionId()] = $section->getBatch() . " " . $section->getYear();
        }
        $response = new JsonResponse();
        $response->setData($sectionIds);
        return $response;
    }

    /**
     * @Route("/teacher/students/{sectionId}/get/", name="teacher.getStudents")
     * @param int|string $sectionId
     * @return JsonResponse
     * @throws \Exception * @internal param string $courseId
     */
    public function getStudentsAction(int $sectionId)
    {
        /** @var Teacher $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Section');
        /** @var Section $section */
        $section = $repo->find($sectionId);
        if ($section->getTeacher()->getUsername() != $user->getUsername()) {
            throw new AccessDeniedException();
        }
        /** @var array $students */
        $students = $section->getStudents();
        $response = new JsonResponse();
        $user_names = array();
        /** @var Student $student */
        foreach ($students as $student) {
            $user_names[] = $student->getUsername();
        }
        $response->setData($user_names);
        return $response;
    }

    /**
     * @Route("/teacher/report/section/{sectionId}", name="teacher.viewSectionReport")
     * @param int $sectionId
     * @return Response
     * @internal param int $assignmentId
     */
    public function viewSectionReport(int $sectionId)
    {
        // Get the user
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // Get the requested assignment from the database
        $repo = $this->getDoctrine()->getRepository('AppBundle:Section');
        /** @var Section $section */
        $section = $repo->find($sectionId);
        if ($section->getTeacher()->getUsername() != $user->getUsername()) {
            throw new AccessDeniedException();
        }
        $assignments = $section->getAssignments();
        /** @var array $studentsGrade */ // k Student v max_grade
        $studentsGrade = [];
        $students = [];
        /** @var Assignment $assignment */
        foreach ($assignments as $assignment) {
            $repo = $this->getDoctrine()->getRepository('AppBundle:Attempt');
            /** @var array $attempts */
            $attempts = $repo->findBy(['assignment' => $assignment->getAssignmentId()]);
            $assignmentID = $assignment->getAssignmentId();
            /** @var Attempt $attempt */
            foreach ($attempts as $attempt) {
                $username = $attempt->getStudent()->getUsername();
                if (array_key_exists($assignmentID, $studentsGrade) && array_key_exists($username, $studentsGrade[$assignmentID])) {
                    $studentsGrade[$assignmentID][$username] = max($studentsGrade[$assignmentID][$username], $attempt->getGrade());
                } else {
                    $studentsGrade[$assignmentID][$username] = $attempt->getGrade();
                }
                if (!in_array($username, $students)) {
                    $students[] = $username;
                }
            }
        }
        $assignmentTitles = [];
        foreach ($assignments as $assignment) {
            $assignmentID = $assignment->getAssignmentId();
            $assignmentTitles[$assignmentID] = $assignment->getTitle();
            foreach ($students as $student) {
                if (!array_key_exists($assignmentID, $studentsGrade) || !array_key_exists($student, $studentsGrade[$assignmentID])) $studentsGrade[$assignmentID][$student] = null;
            }
        }
        return $this->render('@Teacher/Reports/sectionReport.html.twig', ['students' => $studentsGrade, 'labels' => $students, 'titles' => $assignmentTitles]);
    }


    /**
     * @Route("/teacher/report/section/grade/{sectionId}", name="teacher.viewSectionGradeReport")
     * @param int $sectionId
     * @return Response
     * @internal param int $assignmentId
     */
    public function viewSectionGradeReport(int $sectionId)
    {
        // Get the user
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // Get the requested assignment from the database
        $repo = $this->getDoctrine()->getRepository('AppBundle:Section');
        /** @var Section $section */
        $section = $repo->find($sectionId);
        if ($section->getTeacher()->getUsername() != $user->getUsername()) {
            throw new AccessDeniedException();
        }
        $assignments = $section->getAssignments();
        $grades = [];
        /** @var Assignment $assignment */
        foreach ($assignments as $assignment) {
            $repo = $this->getDoctrine()->getRepository('AppBundle:Attempt');
            /** @var array $attempts */
            $attempts = $repo->findBy(['assignment' => $assignment->getAssignmentId()]);
            /** @var array $studentsGrade */ // k Student v max_grade
            $studentsGrade = [];
            /** @var Attempt $attempt */
            foreach ($attempts as $attempt) {
                $username = $attempt->getStudent()->getUsername();
                if (array_key_exists($username, $studentsGrade)) {
                    $studentsGrade[$username] = max($studentsGrade[$username], $attempt->getGrade());
                } else {
                    $studentsGrade[$username] = $attempt->getGrade();
                }
            }
            foreach ($studentsGrade as $username => $grade) {
                if (array_key_exists($grade, $grades)) {
                    $grades[intdiv($grade, 5)] = $grades[intdiv($grade, 5)] + 1;
                } else {
                    $grades[intdiv($grade, 5)] = 1;
                }
            }
        }
        ksort($grades);
        return $this->render('@Teacher/Reports/sectionGradeReport.html.twig', ['grades' => $grades]);
    }

    /**
     * @Route("/teacher/report/student/{sectionId}/{username}", name="teacher.viewStudentReport")
     * @param string $username
     * @param int $sectionId
     * @return Response
     * @internal param int $sectionId
     * @internal param int $assignmentId
     */
    public function viewStudentReport(string $username, int $sectionId)
    {
        // Get the user
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // Get the requested assignment from the database
        $repo = $this->getDoctrine()->getRepository('AppBundle:Section');
        /** @var Section $section */
        $section = $repo->find($sectionId);
        if ($section->getTeacher()->getUsername() != $user->getUsername()) {
            throw new AccessDeniedException();
        }
        $assignments = $section->getAssignments();
        /** @var array $students */ // k Student v total grade
        $stu_assignments = array();
        $assignmentTitles = [];
        /** @var Assignment $assignment */
        foreach ($assignments as $assignment) {
            $repo = $this->getDoctrine()->getRepository('AppBundle:Attempt');
            $assignmentID = $assignment->getAssignmentId();
            $assignmentTitles[$assignmentID] = $assignment->getTitle();
            /** @var array $attempts */
            $attempts = $repo->findBy(['assignment' => $assignmentID, 'student' => $username]);
            /** @var Attempt $attempt */
            foreach ($attempts as $attempt) {
                if (array_key_exists($assignmentID, $stu_assignments)) {
                    $stu_assignments[$assignmentID] = max($stu_assignments[$assignmentID], $attempt->getGrade());
                } else {
                    $stu_assignments[$assignmentID] = $attempt->getGrade();
                }
            }
        }
        return $this->render('@Teacher/Reports/studentReport.html.twig', ['assignments' => $stu_assignments, 'titles'=>$assignmentTitles]);
    }

    // /------------------------------------------PROFILE---------------------------------------------------------------

    /**
     * @Route("/teacher/profile/update", name="teacher.updateProfile")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateProfileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Teacher $teacher */
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        $entity = clone($teacher);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $form = $this->createForm(TeacherType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $temp = clone($teacher);

            $teacher->setUsername($entity->getUsername());
            $teacher->setEmail($entity->getEmail());
            $teacher->setLastName($entity->getLastName());
            $teacher->setFirstName($entity->getFirstName());
            $teacher->setPassword($entity->getPassword());


            // Encode password
            $teacher->encodePassword($this->container);

            try {
                $em->persist($teacher);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated successfully.');
                return $this->redirectToRoute('teacher.dashboard');
            } catch (DBALException $ex) {
                // $form->get('username')->addError(new FormError('There is a person with the same username.'));
                $form->get('email')->addError(new FormError('There is a person with the same e-mail address.'));

                $teacher->setUsername($temp->getUsername());
                $teacher->setEmail($temp->getEmail());
                $teacher->setLastName($temp->getLastName());
                $teacher->setFirstName($temp->getFirstName());
                $teacher->setPassword($temp->getPassword());
            }
        }

        return $this->render('@Teacher/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}