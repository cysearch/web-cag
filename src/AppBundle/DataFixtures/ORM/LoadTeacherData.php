<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 4/30/2016
 * Time: 11:37 AM
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Model\Institute\Teacher;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTeacherData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $teacher = new Teacher();
        // change object attributes
        $teacher->setUsername('teacher');
        $teacher->setEmail('teacher@gmail.lk');
        $teacher->setFirstName('teacher');
        $teacher->setLastName('1');
        $teacher->setPassword('$2a$12$yhVFD66AEd7Wo8.1WIpCPuP0a29iaeOQTmAwAACMzO6SF1rubOtSO');
        $teacher->setIsActive(true);
        // Add the values to the database
        $manager->persist($teacher);
        $manager->flush();
        $this->addReference('teacher', $teacher);
    }

    public function getOrder()
    {
        return 2;
    }
}