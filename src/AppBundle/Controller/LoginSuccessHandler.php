<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected
        $router,
        $security;

    public function __construct(Router $router, AuthorizationChecker $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        //'ROLE_TEACHER''ROLE_ADMIN''ROLE_STUDENT'
        // Default target for unknown roles. Everyone else go there.
        $url = 'login';
        if($this->security->isGranted('ROLE_STUDENT')) {
            $url = 'student.dashboard';
        }
        elseif($this->security->isGranted('ROLE_TEACHER')) {
            $url = 'teacher.dashboard';
        }
        elseif($this->security->isGranted('ROLE_ADMIN')) {
            $url = 'admin.dashboard';
        }
        $response = new RedirectResponse($this->router->generate($url));

        return $response;
    }
}