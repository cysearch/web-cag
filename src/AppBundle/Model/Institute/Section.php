<?php

namespace AppBundle\Model\Institute;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Section
 *
 * @ORM\Table(name="section", uniqueConstraints={@ORM\UniqueConstraint(name="section_unique", columns={"course_id", "year", "batch_id", "teacher_id"})})
 * @ORM\Entity
 */
class Section {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="section_id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $sectionId;

	/**
	 * @var string
	 *
	 * @ORM\ManyToOne(targetEntity="Course", inversedBy="sections")
	 * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id", nullable=false, unique=false)
     *
     * @Assert\NotBlank()
	 */
	private $course;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="year", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1700,
     *      max = 2300,
     *      minMessage = "Year must be at least {{ limit }}.",
     *      maxMessage = "Year must be at most {{ limit }}."
     * )
	 */
	private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="batch_id", type="string", length=20)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 1,
     *      max = 20,
     *      minMessage = "Batch reference must be at least {{ limit }} characters long",
     *      maxMessage = "Batch reference cannot be longer than {{ limit }} characters"
     * )
     */
    private $batch;

	/**
	 * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="sections")
	 * @ORM\JoinColumn(name="teacher_id", referencedColumnName="username", unique=false)
     *
     * @Assert\NotBlank()
	 */
	private $teacher;

    /**
     * @ORM\OneToMany(targetEntity="Assignment", mappedBy="section")
     */
    private $assignments;

    /**
     * @ORM\ManyToMany(targetEntity="Student", mappedBy="sections")
     */
    private $students;

    /**
     * Get sectionId
     *
     * @return integer
     */
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Section
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set batch
     *
     * @param string $batch
     *
     * @return Section
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return string
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * Set course
     *
     * @param Course $course
     *
     * @return Section
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get Course
     *
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set Teacher
     *
     * @param \AppBundle\Model\Institute\Teacher $teacher
     *
     * @return Section
     */
    public function setTeacher(Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get Teacher
     *
     * @return \AppBundle\Model\Institute\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Gets the essence and create a string
     *
     * @return string
     */
    public function toString() {
        return $this->getCourse()->getCourseId() . " ". $this->getYear() . " " . $this->getBatch();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assignments = new ArrayCollection();
    }

    /**
     * Add assignment
     *
     * @param \AppBundle\Model\Institute\Assignment $assignment
     *
     * @return Section
     */
    public function addAssignment(Assignment $assignment)
    {
        $this->assignments[] = $assignment;

        return $this;
    }

    /**
     * Remove assignment
     *
     * @param \AppBundle\Model\Institute\Assignment $assignment
     */
    public function removeAssignment(Assignment $assignment)
    {
        $this->assignments->removeElement($assignment);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignments()
    {
        return $this->assignments;
    }

    /**
     * Add student
     *
     * @param \AppBundle\Model\Institute\Student $student
     *
     * @return Section
     */
    public function addStudent(Student $student)
    {
        $this->students[] = $student;

        return $this;
    }

    /**
     * Remove student
     *
     * @param \AppBundle\Model\Institute\Student $student
     */
    public function removeStudent(Student $student)
    {
        $this->students->removeElement($student);
    }

    /**
     * Get students
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudents()
    {
        return $this->students;
    }
}
