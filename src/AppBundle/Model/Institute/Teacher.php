<?php

namespace AppBundle\Model\Institute;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="teacher")
 */
class Teacher extends Person
{
    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="teacher")
     */
    private $sections;

    /**
     * Returns the roles granted to the user.
     * @return array (Role|string)[] The user roles
     */
    public function getRole()
    {
        return ['ROLE_TEACHER'];
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->getRole();
    }

    /**
     * Add section
     *
     * @param \AppBundle\Model\Institute\Section $section
     *
     * @return Teacher
     */
    public function addSection(\AppBundle\Model\Institute\Section $section)
    {
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Remove section
     *
     * @param \AppBundle\Model\Institute\Section $section
     */
    public function removeSection(\AppBundle\Model\Institute\Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }
}
