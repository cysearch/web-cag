<?php

namespace AppBundle\Model\Institute;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="answer")
 * @ORM\Entity
 */
class Answer {
    /**
     * @var int
     *
     * @ORM\Column(name="answer_id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $answerId;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="question_id", nullable=false)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text")
     */
    private $script;

    /**
     * @var int
     *
     * @ORM\Column(name="grade", type="integer", unique=false, nullable=false)
     *
     */
    private $grade;
    
    /**
     * Set answerId
     *
     * @param integer $answerId
     *
     * @return Answer
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return Answer
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Model\Institute\question $question
     *
     * @return Answer
     */
    public function setQuestion(\AppBundle\Model\Institute\question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Model\Institute\question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     *
     * @return Answer
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade()
    {
        return $this->grade;
    }
}
