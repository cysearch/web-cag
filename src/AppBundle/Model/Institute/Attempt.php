<?php

namespace AppBundle\Model\Institute;

use AppBundle\Model\Grader\PythonSourceCodeTester;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="attempt")
 * @ORM\Entity
 */
class Attempt {
    /**
     * @var int
     *
     * @ORM\Column(name="attempt_id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $attemptId;
    
    /**
     * @var Student
     *
     * @ORM\ManyToOne(targetEntity="Student")
     * @ORM\JoinColumn(name="username", referencedColumnName="username", nullable=false)
     */
    private $student;

    /**
     * @var Assignment
     *
     * @ORM\ManyToOne(targetEntity="Assignment")
     * @ORM\JoinColumn(name="assignment_id", referencedColumnName="assignment_id", nullable=false)
     */
    private $assignment;

    /**
     * @var int
     *
     * @ORM\Column(name="attempt_no", type="integer", unique=false)
     *
     */
    private $attemptNo;

    /**
     * @ORM\ManyToMany(targetEntity="Answer")
     * @ORM\JoinTable(name="attempt_answer",
     *      joinColumns={@ORM\JoinColumn(name="attempt_id", referencedColumnName="attempt_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="answer_id", referencedColumnName="answer_id")})
     */
    private $answers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Get attemptId
     *
     * @return integer
     */
    public function getAttemptId()
    {
        return $this->attemptId;
    }

    /**
     * Set attemptNo
     *
     * @param integer $attemptNo
     *
     * @return Attempt
     */
    public function setAttemptNo($attemptNo)
    {
        $this->attemptNo = $attemptNo;

        return $this;
    }

    /**
     * Get attemptNo
     *
     * @return integer
     */
    public function getAttemptNo()
    {
        return $this->attemptNo;
    }

    /**
     * Set student
     *
     * @param Student $student
     *
     * @return Attempt
     */
    public function setStudent(Student $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \AppBundle\Model\Institute\student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set assignment
     *
     * @param Assignment $assignment
     *
     * @return Attempt
     */
    public function setAssignment(Assignment $assignment)
    {
        $this->assignment = $assignment;

        return $this;
    }

    /**
     * Get assignment
     *
     * @return Assignment
     */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * Add answer
     *
     * @param Answer $answer
     *
     * @return Attempt
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param Answer $answer
     */
    public function removeAnswer(Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade()
    {
        $count = sizeof($this->getAssignment()->getQuestions());
        if ($count == 0) return 0;
        $grade = 0;
        /** @var Answer $answer */
        foreach ($this->answers as $answer) {
            if ($this->getAssignment()->getQuestions()->contains($answer->getQuestion())) {
                $grade += $answer->getGrade();
            }
        }
        return ($grade / 100) * $this->getAssignment()->getTotalMarksAllowed() / $count;
    }

    public function updateGrade(ObjectManager $em) {
        foreach ($this->getAnswers() as $answer) {
            /**
             * Calculate Grade and set grade value of student for each question
             */
            if ($answer->getScript() == null) {
                $answer->setScript("");
            }
            $PythonTester = new PythonSourceCodeTester();
            $results = $PythonTester->gradeScripOnTestCases($answer->getScript(),
                $answer->getQuestion()->getTestCases());
            /**
             * -----------------------------------------------------------------
             */
            $answer->setGrade($results);
            $em->persist($answer);
            $em->flush();
        }
    }
}
