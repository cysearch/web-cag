<?php

namespace AppBundle\Model\Institute;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="question")
 */
class Question {
    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $questionId;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="questions")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id", nullable=false)
     */
    private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 1,
     *      max = 2500,
     *      minMessage = "Batch reference must be at least {{ limit }} characters long",
     *      maxMessage = "Batch reference cannot be longer than {{ limit }} characters"
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="test_cases", type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 1,
     *      max = 2500,
     *      minMessage = "Batch reference must be at least {{ limit }} characters long",
     *      maxMessage = "Batch reference cannot be longer than {{ limit }} characters"
     * )
     */
    private $testCases;

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Question
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set testCases
     *
     * @param string $testCases
     *
     * @return Question
     */
    public function setTestCases($testCases)
    {
        $this->testCases = $testCases;

        return $this;
    }

    /**
     * Get testCases
     *
     * @return string
     */
    public function getTestCases()
    {
        return $this->testCases;
    }

    public function toString() {
        return $this->description;
    }

    /**
     * Set course
     *
     * @param Course $course
     *
     * @return Question
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \AppBundle\Model\Institute\Course
     */
    public function getCourse()
    {
        return $this->course;
    }
}
