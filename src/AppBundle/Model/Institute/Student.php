<?php

namespace AppBundle\Model\Institute;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="student")
 */
class Student extends Person {

	/**
	 * @ORM\ManyToMany(targetEntity="Section", inversedBy="students")
     * @ORM\JoinTable(name="enrollment",
     *      joinColumns={@ORM\JoinColumn(name="index_no", referencedColumnName="username")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="section_id")}
     *      )
	 */
	private $sections;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="batch", type="string", length=10, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "Batch id must be at least {{ limit }} characters long",
     *      maxMessage = "Batch id can't be longer than {{ limit }} characters"
     * )
	 */
	private $batch;

	/**
	 * Returns the roles granted to the user.
	 * @return array (Role|string)[] The user roles
	 */
	public function getRole()
	{
		return ['ROLE_STUDENT'];
	}

    /**
     * Add section
     *
     * @param Section $section
     *
     * @return Student
     */
    public function addSection(Section $section)
    {
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Remove section
     *
     * @param Section $section
     */
    public function removeSection(Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Set batch
     *
     * @param string $batch
     *
     * @return Student
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return string
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->getRole();
    }

    /**
     *  Sets the sections
     * @param $sections
     */
    public function setSection($sections) {
        $this->sections = $sections;
    }
}
