<?php

namespace AppBundle\Model\Institute;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="assignment")
 * @ORM\Entity
 */
class Assignment
{
    /**
     * @var int
     *
     * @ORM\Column(name="assignment_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $assignmentId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Title must be at least {{ limit }} characters long",
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Description must be at least {{ limit }} characters long",
     *      maxMessage = "Description cannot be longer than {{ limit }} characters"
     * )
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Question")
     * @ORM\JoinTable(name="assignment_question",
     *      joinColumns={@ORM\JoinColumn(name="assignment_id", referencedColumnName="assignment_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="question_id")})
     */
    private $questions;

    /**
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="assignments")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="section_id", nullable=false)
     */
    private $section;


    /**
     * @var int
     *
     * @ORM\Column(name="attempts_allowed", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      max = 20,
     *      minMessage = "No of attempts allowed must be at least {{ limit }}.",
     *      maxMessage = "No of attempts allowed must be at most {{ limit }}."
     * )
     */
    private $attemptsAllowed;

    /**
     * @var date $enableDate
     *
     * @ORM\Column(name="enable_date", type="date")
     *
     * @Assert\DateTime()
     */
    private $enableDate;

    /**
     * @var date $disableDate
     *
     * @ORM\Column(name="disable_date", type="date")
     *
     * @Assert\DateTime()
     */
    private $disableDate;

    /**
     * @var int
     *
     * @ORM\Column(name="total_marks_allowed", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      minMessage = "Total allowed marks per assignment must be at least {{ limit }}.",
     *      maxMessage = "Total allowed marks per assignment must be at most {{ limit }}."
     * )
     */
    private $totalMarksAllowed;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * Get assignmentId
     *
     * @return integer
     */
    public function getAssignmentId()
    {
        return $this->assignmentId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Assignment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Assignment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Model\Institute\question $question
     *
     * @return Assignment
     */
    public function addQuestion(Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Model\Institute\question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set section
     *
     * @param Section $section
     *
     * @return Assignment
     */
    public function setSection(Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Model\Institute\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set attemptsAllowed
     *
     * @param integer $attemptsAllowed
     *
     * @return Assignment
     */
    public function setAttemptsAllowed($attemptsAllowed)
    {
        $this->attemptsAllowed = $attemptsAllowed;

        return $this;
    }

    /**
     * Get attemptsAllowed
     *
     * @return integer
     */
    public function getAttemptsAllowed()
    {
        return $this->attemptsAllowed;
    }

    /**
     * Set enableDate
     *
     * @param \DateTime $enableDate
     *
     * @return Assignment
     */
    public function setEnableDate($enableDate)
    {
        $this->enableDate = $enableDate;

        return $this;
    }

    /**
     * Get enableDate
     *
     * @return \DateTime
     */
    public function getEnableDate()
    {
        return $this->enableDate;
    }

    /**
     * Set disableDate
     *
     * @param \DateTime $disableDate
     *
     * @return Assignment
     */
    public function setDisableDate($disableDate)
    {
        $this->disableDate = $disableDate;

        return $this;
    }

    /**
     * Get disableDate
     *
     * @return \DateTime
     */
    public function getDisableDate()
    {
        return $this->disableDate;
    }

    /**
     * Set totalMarksAllowed
     *
     * @param integer $totalMarksAllowed
     *
     * @return Assignment
     */
    public function setTotalMarksAllowed($totalMarksAllowed)
    {
        $this->totalMarksAllowed = $totalMarksAllowed;

        return $this;
    }

    /**
     * Get totalMarksAllowed
     *
     * @return integer
     */
    public function getTotalMarksAllowed()
    {
        return $this->totalMarksAllowed;
    }

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     */
    public function isStartBeforeEnd(ExecutionContextInterface $context)
    {
        if ($this->getEnableDate() > $this->getDisableDate()) {
            $context->buildViolation('The enable date must be prior to the disable date.')
                ->atPath('enableDate')
                ->addViolation();
        }
    }

    public function updateAttemptedGrades(Registry $dr)
    {
        $repo = $dr->getRepository('AppBundle:Attempt');
        $attempts = $repo->findBy(['assignment' => $this->getAssignmentId()]);
        $em = $dr->getManager();
        foreach ($attempts as $attempt) {
            $attempt->updateGrade($em);
        }
    }
}
