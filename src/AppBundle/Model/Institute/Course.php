<?php

namespace AppBundle\Model\Institute;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="course")
 * @ORM\Entity
 */
class Course
{
    /**
     * @var string
     *
     * @ORM\Column(name="course_id", type="string", length=7)
     * @ORM\Id
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 45,
     *      minMessage = "Title must be at least {{ limit }} characters long",
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;


    /**
     * @var int
     *
     * @ORM\Column(name="semester", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      max = 8,
     *      minMessage = "Semester must be at least {{ limit }}.",
     *      maxMessage = "Semester must be at most {{ limit }}."
     * )
     */
    private $semester;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="course")
     */
    private $questions;


    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="course")
     */
    private $sections;

    /**
     * Set courseId
     *
     * @param string $courseId
     *
     * @return Course
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return string
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Course
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add question
     *
     * @param \AppBundle\Model\Institute\Question $question
     *
     * @return Course
     */
    public function addQuestion(\AppBundle\Model\Institute\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Model\Institute\Question $question
     */
    public function removeQuestion(\AppBundle\Model\Institute\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add section
     *
     * @param \AppBundle\Model\Institute\Section $section
     *
     * @return Course
     */
    public function addSection(\AppBundle\Model\Institute\Section $section)
    {
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Remove section
     *
     * @param \AppBundle\Model\Institute\Section $section
     */
    public function removeSection(\AppBundle\Model\Institute\Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }
}
