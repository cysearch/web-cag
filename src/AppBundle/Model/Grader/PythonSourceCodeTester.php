<?php
/**
 * Created by PhpStorm.
 * User: Yasas
 * Date: 4/17/2016
 * Time: 6:05 PM
 */

namespace AppBundle\Model\Grader;

use AppBundle\Model\Exceptions\ParseException;
use AppBundle\Model\Helper\FileManager;
use AppBundle\Model\Helper\Parameters;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\ProcessBuilder;

class PythonSourceCodeTester extends SourceCodeTester
{

    /**
     * PythonSourceCodeTester constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $script
     * @param string $test
     * @return float|void
     */
    public function gradeScripOnTestCases(string $script, string $test)
    {
        $results = $this->runTests($script, $test);
        $testOutput = $results['out'];
        $testErr = $results['err'];
//        throw new Exception($testErr . " " . $testOutput);
        if ($testOutput == "") {
            return 0;
        }
        try {
            $parsedTestOutput = $this->parseTestOutput($testOutput);
            $testCount = $parsedTestOutput['total'];
            $sumOfResults = $parsedTestOutput['errors'] * 10 + $parsedTestOutput['failures'] * 7 + $parsedTestOutput['skipped'] * 5;
            return ($testCount * 10 - $sumOfResults) * 100 / ($testCount * 10);
        } catch (ParseException $e) {
            return 0;
        }
    }

    /**
     * Run tests on script
     * @param $script string python Script to be tested
     * @param $test string test script
     * @return array
     */
    public function runTests(string $script, string $test)
    {
        $base_dir = Parameters::readValue('base_dir');
        // Create the file in the server - Open and write finally close
        $fm = new FileManager($base_dir);
        // Make text python compatible
        $script = $this->pythonCompatibleText($script);
        $fm->createFile("source1.py", $script);
        // create the test file content using the test file template
        $test = $this->createTestString($test);
        $fm->createFile("test1.py", $test);
        // Execute python script in the server and get output ane error
        // $command = "python " . $base_dir . "\\test1.py";
        // $process = new Process($command);
        $builder = new ProcessBuilder();
        $process = $builder->setPrefix('python')
            ->setArguments(array($base_dir . "\\test1.py"))
            ->setTimeout(5)
            ->getProcess();
        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
        } catch (ProcessTimedOutException $e) {
        }
        return ['out' => $process->getOutput(), 'err' => $process->getErrorOutput()];
    }

    /**
     * Checks syntax (errors)
     *
     * @param string $script
     * @return array
     */
    public  function checkSyntax(string $script) {
        $base_dir = Parameters::readValue('base_dir');
        // Create the file in the server - Open and write finally close
        $script = $this->pythonCompatibleText($script);
        // create the test file content using the test file template
//        $test = $this->createTestString($script);
        $fm = new FileManager($base_dir);
        // Make text python compatible
        $fm->createFile("script.py", $script);
        $builder = new ProcessBuilder();
        // python -m py_compile script.py
        $process = $builder->setPrefix('python')
            ->setArguments(["-m" , "py_compile",  $base_dir . "\\script.py"])
            ->setTimeout(5)
            ->getProcess();
        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            return ['out' => $process->getOutput(), 'err' =>  'Process Failed! ' . $process->getErrorOutput()];
        } catch (ProcessTimedOutException $e) {
            return ['out' => '', 'err' => 'Process Timed Out!'];
        }
        return ['out' => $process->getOutput(), 'err' => $process->getErrorOutput()];
    }

    /**
     * Checks syntax (errors)
     *
     * @param string $script
     * @return array
     */
    public  function runScript(string $script) {
        $base_dir = Parameters::readValue('base_dir');
        // Create the file in the server - Open and write finally close
        $script = $this->pythonCompatibleText($script);
        $fm = new FileManager($base_dir);
        // Make text python compatible
        $fm->createFile("script.py", $script);
        $builder = new ProcessBuilder();
        $process = $builder->setPrefix('python')
            ->setArguments([$base_dir . "\\script.py"])
            ->setTimeout(5)
            ->getProcess();
        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            return ['out' => $process->getOutput(), 'err' =>  'Process Failed! ' . $process->getErrorOutput()];
        } catch (ProcessTimedOutException $e) {
            return ['out' => '', 'err' => 'Process Timed Out!'];
        }
        return ['out' => $process->getOutput(), 'err' => $process->getErrorOutput()];
    }

    /**
     * parse json like test output
     * @param string $testOutput
     * @return array
     */
    public static function parseTestOutput(string $testOutput)
    {
        if ($testOutput == '') throw new ParseException("Empty Output");
//        $debug = $testOutput;
        $testOutput = explode("#testResults ", $testOutput);
        $results = explode(";", end($testOutput));
        $output = Array();
        for ($i = 0; $i < count($results); $i++) {
            $property = explode(":", $results[$i]);
            if (count($property) != 2) throw new ParseException("Invalid parameter length.");
            $output[$property[0]] = $property[1];
        }
        return $output;
    }

    /**
     * Combines test cases with the unit test template
     * @param string $text
     * @return string
     */
    private function createTestString(string $text)
    {
        $template = FileManager::readTemplate('python_test_template.txt');
        $text = str_replace("\n", "\n\t", $text);
        $testClass = str_replace('#Tests', $text, $template);
        $testClass = $this->pythonCompatibleText($testClass);
        return $testClass;
    }

    /**
     * Changes encoding style manually replacing /r/n with /n
     * Improves compatibility with python interpreter
     * @param string $text
     * @return string
     */
    private function pythonCompatibleText(string $text)
    {
        $text = str_replace("\t", "    ", $text);
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\n", "\r\n", $text);
        $text = mb_convert_encoding($text, 'ascii');
        return $text;
    }
}