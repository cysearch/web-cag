<?php

namespace StudentBundle\Controller;

use AppBundle\Model\Grader\PythonSourceCodeTester;
use AppBundle\Model\Institute\Answer;
use AppBundle\Model\Institute\Attempt;
use AppBundle\Model\Institute\Section;
use AppBundle\Model\Institute\Student;
use Doctrine\DBAL\DBALException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StudentBundle\Form\AttemptType;
use StudentBundle\Form\StudentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends Controller
{
    /**
     * @Route("/student", name="student.dashboard")
     */
    public function indexAction()
    {
        $student = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('StudentBundle:Default:index.html.twig', ['sections' => $student->getSections()]);
    }

    /**
     * @Route("/student/sections", name="student.viewSections")
     */
    public function viewSectionsAction()
    {
        $student = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('@Student/Sections/sections.html.twig', ['sections' => $student->getSections()]);
    }

    /**
     * @Route("/student/section/{sectionId}/assignments", name="student.viewAssignments")
     * @param int $sectionId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function viewAssignmentsAction(int $sectionId)
    {
        $student = $this->get('security.token_storage')->getToken()->getUser();

        /** @var Section $section */
        foreach ($student->getSections() as $section) {
            if ($section->getSectionId() == $sectionId) {
                $assignments = $section->getAssignments();
                return $this->render('StudentBundle:Assignments:assignments.html.twig', ['assignments' => $assignments, 'sectionId' => $sectionId, 'sections' => $student->getSections()]);
            }
        }

        throw $this->createNotFoundException('Section not avalable.');
    }

    /**
     * @Route("/student/assignment/{assignmentId}", name="student.viewAssignment")
     * @param int $assignmentId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAssignmentAction(int $assignmentId)
    {
        $student = $this->get('security.token_storage')->getToken()->getUser();

        $assignment = $this->getDoctrine()
            ->getRepository('AppBundle:Assignment')
            ->find($assignmentId);

        if (!$assignment) {
            throw $this->createNotFoundException('Assignment not avalable.');
        }

        $attempts = $this->getDoctrine()
            ->getRepository('AppBundle:Attempt')
            ->findBy(['student' => $student, 'assignment' => $assignment]);

        if ($student->getSections()->contains($assignment->getSection())) {
            return $this->render('StudentBundle:Assignments:assignment.html.twig', ['assignment' => $assignment,
                'attempts' => $attempts,
                'attemptsLeft' => $assignment->getAttemptsAllowed() - sizeof($attempts),
                'sections' => $student->getSections()]);
        }

        throw $this->createNotFoundException('You are not authorized to view this assignment.');
    }

    /**
     * @Route("/student/assignment/{assignmentId}/attempt", name="student.attemptAssignment")
     * @param Request $request
     * @param int $assignmentId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function attemptAssignmentAction(Request $request, int $assignmentId)
    {
        $student = $this->get('security.token_storage')->getToken()->getUser();

        $assignment = $this->getDoctrine()
            ->getRepository('AppBundle:Assignment')
            ->find($assignmentId);

        if (!$assignment) {
            throw $this->createNotFoundException('Assignment not avalable.');
        }

        $attempts = $this->getDoctrine()
            ->getRepository('AppBundle:Attempt')
            ->findBy(['student' => $student, 'assignment' => $assignment]);

        // Is the assignment owned by the student
        if ($student->getSections()->contains($assignment->getSection())) {
            if ($assignment->getEnableDate() > new \DateTime('now')) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Try again when the assignment is enabled.');
            } elseif ($assignment->getDisableDate() < new \DateTime('now')) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Sorry, The deadline for the assignment has already passed.');
            } else {
                if ($assignment->getAttemptsAllowed() <= sizeof($attempts)) {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('error', 'You have no more attempts remaining.');
                } else {
                    // Create new attempt
                    $attempt = new Attempt();
                    // Set assignment of the attempt
                    $attempt->setAssignment($assignment);
                    foreach ($assignment->getQuestions() as $question) {
                        $answer = new Answer();
                        $attempt->setStudent($student);
                        $attempt->setAttemptNo(sizeof($attempts) + 1);
                        $answer->setQuestion($question);
                        $attempt->addAnswer($answer);
                    }
                    // Create form for answering the assignment
                    $form = $this->createForm(AttemptType::class, $attempt);

                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        try {
                            $em = $this->getDoctrine()->getManager();
                            // Update grade before storing
                            $attempt->updateGrade($em);
                            $em->persist($attempt);
                            $em->flush();
                            $request->getSession()
                                ->getFlashBag()
                                ->add('success', 'You have successfully submitted the assignment.');
                            return $this->redirectToRoute('student.viewAssignment', ['assignmentId' => $assignment->getAssignmentId(),
                                'sections' => $student->getSections()]);
                        } catch (DBALException $ex) {
                            $form->get('answers')->addError(new FormError('Malicious code segment detected please try again.(Your results did not get recorded.)'));
                        }
                    }
                    // Show the form
                    return $this->render('@Student/Attempt/attempt.html.twig', array(
                        'form' => $form->createView(),
                        'attempt' => $attempt,
                        'sections' => $student->getSections()
                    ));
                }
                return $this->redirectToRoute('student.viewAssignment', ['assignmentId' => $assignment->getAssignmentId(),
                    'sections' => $student->getSections()]);
            }
            return $this->redirectToRoute('student.viewAssignment', ['assignmentId' => $assignment->getAssignmentId(),
                'sections' => $student->getSections()]);
        }
        throw $this->createNotFoundException('You are not authorized to request this assignment.');
    }

    // /---------------------------------------CODE TESTER--------------------------------------------------------------

    /**
     * @Route("/student/runScript", name="student.runScript")
     * @param Request $request
     * @return Response
     */
    public function runScriptAction(Request $request) {
        // $_POST parameters
        $script = $request->request->get('script');
        $sourceCodeTester = new PythonSourceCodeTester();
        $testResults = $sourceCodeTester->checkSyntax($script);
        if ($testResults['out'] != "" || $testResults['err'] != "") {
            $splitErr = explode(',', $testResults['err'], 2);
            if (sizeof($splitErr) == 2) {
                $error = explode(' ', $splitErr[1], 4);
                $error_line = $error[2];
                $err_msg = $error[3];
                return new Response("Error at line no: " . $error_line . " :- " . $err_msg);
            } else {
                return new Response("Syntax errors found. Please check the code again.");
            }
        }
        return new Response("There are no syntax errors!");
    }

    // /------------------------------------------PROFILE---------------------------------------------------------------

    /**
     * @Route("/student/profile/update", name="student.updateProfile")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateProfileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Student $student */
        $student = $this->get('security.token_storage')->getToken()->getUser();

        $entity = clone($student);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $form = $this->createForm(StudentType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $temp = clone($student);

            $student->setUsername($entity->getUsername());
            $student->setEmail($entity->getEmail());
            $student->setLastName($entity->getLastName());
            $student->setFirstName($entity->getFirstName());
            $student->setPassword($entity->getPassword());
            $student->setSection($entity->getSections());

            // Encode password
            $student->encodePassword($this->container);

            try {
                $em->persist($student);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Updated successfully.');
                return $this->redirectToRoute('student.dashboard');
            } catch (DBALException $ex) {
                // $form->get('username')->addError(new FormError('There is a person with the same username.'));
                $form->get('email')->addError(new FormError('There is a person with the same e-mail address.'));

                $student->setUsername($temp->getUsername());
                $student->setEmail($temp->getEmail());
                $student->setLastName($temp->getLastName());
                $student->setFirstName($temp->getFirstName());
                $student->setPassword($temp->getPassword());

            }
        }

        return $this->render('@Student/Default/basicForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}
