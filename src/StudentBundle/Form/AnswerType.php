<?php

namespace StudentBundle\Form;

use AppBundle\Model\Institute\Answer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('script', TextareaType::class, array('label' => 'Question is read protected!'));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Answer $answer */
            $answer = $event->getData();
            $form = $event->getForm();
            if (!$answer || null != $answer->getQuestion()) {
                $question = $answer->getQuestion();
                $form->add('script', TextareaType::class, array('label' => $question->getDescription()));
            } else {
                $form->add('script', TextareaType::class, array('label' => 'Question is read protected!'));
            }
        });
    }
}