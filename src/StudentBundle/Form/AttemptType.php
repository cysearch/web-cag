<?php

namespace StudentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AttemptType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('answers', CollectionType::class, array(
                'entry_type' => AnswerType::class,
                'entry_options' => [
                    'data_class' => 'AppBundle\Model\Institute\Answer'
                ],
                'label' => 'Answers'
            ))
            ->add('submit', SubmitType::class, array('label' => 'Finish', 'attr' => array('class' => 'hidden')));
    }
}